# Celery Rust Asynchronous Client
[![pipeline status](https://gitlab.com/asevans48/datacannon-rs-client/badges/master/pipeline.svg)](https://gitlab.com/asevans48/datacannon-rs-client/-/commits/master)

An asynchronous client using tokio and lapin. For processing batches of work over the same client ot conserve resources.

## Current Support

A Kombu-like port will need to be written for this library. Until then, RabbitMQ and 
anything supporting AMQP will work as a broker. Backends use the BackendConfig.

I really need this library for an existing time-sensitive project though.

## License


Copyright 2019- Andrew Evans

Any use, distribution, redistribution, copying, manipulating, or handling of this software may only be done with the
express permission of the copywrite holder. Freedom of access to this software may change at any stated time unless
stated directly in a written contract. Unauthorized use of this software is prohibited without the express consent of
the author. Derived works shall be considered a part of the original work and are subject to the terms of this license.
