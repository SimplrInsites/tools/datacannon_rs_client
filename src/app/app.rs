//! Client application for sending tasks to and interacting with the backend. Specify one broker
//! and one backend type for each client. They may be the same.
//!
//! ---
//! author: Andrew Evans
//! ---

use std::cell::RefCell;
use std::collections::{HashMap, HashSet};
use std::io::Write;
use std::sync::Arc;

use chrono::Local;
use datacannon_rs_core::argparse::argtype::ArgType;
use datacannon_rs_core::backend::redis::backend::RedisResultHandler;
use datacannon_rs_core::backend::types::AvailableBackend;
use datacannon_rs_core::broker::amqp::rabbitmq::RabbitMQBroker;
use datacannon_rs_core::config::config::BrokerType;
use datacannon_rs_core::config::config::CannonConfig;
use datacannon_rs_core::message_protocol::stream::StreamConfig;
use datacannon_rs_core::result::asynchronous::AsyncResult;
use datacannon_rs_core::result::callbacks::callback::Callback;
use datacannon_rs_core::result::callbacks::errback::Errback;
use datacannon_rs_core::task::config::{TaskConfig, TaskConfigBuilder};
use datacannon_rs_core::task::result::TaskResponse;
use env_logger::Builder;
use lapin::ExchangeKind;
use log::{debug, info, LevelFilter};
use tokio::sync::{Mutex as TokioMutex, RwLock};
use tokio::sync::mpsc::Sender;
use uuid::Uuid;

use crate::app::config::GeneralConfig;
use crate::enums::backends::ClientBackend;
use crate::enums::brokers::ClientBroker;
use crate::host::utils::get_hostname_str;
use tokio::runtime::Runtime;

/// The Application Builder
///
/// # Variables
/// * `app_name` - Name of the application
/// * `broker` - Broker Type to start based on the configuration
/// * `Backend` - An Available Backend
/// * `config` - Configuration for the application
/// * `backend_consumer_ids` - Optional consumer ids to control how ids are sent
/// * `general_config` - The general configuration
///
/// The length of `backend_consumer_ids` must be the same as the number of consumers specified
/// in the backend configuration.
pub struct AppBuilder{
    app_name: &'static str,
    host_name: String,
    broker: Option<BrokerType>,
    backend: Option<AvailableBackend>,
    config: Option<CannonConfig<'static>>,
    backend_consumer_ids: Option<HashSet<String>>,
    general_config: GeneralConfig,
    result_store: Arc<RwLock<HashMap<String, Arc<TokioMutex<Sender<TaskResponse>>>>>>
}


/// App Builder implementation
impl AppBuilder{

    /// Application name. Must be specified and should be unique. Used for reply_to queues
    /// and other information.
    ///
    /// # Arguments
    /// * `app_name` - The application name
    pub fn app_name(&mut self, app_name: &'static str) -> &mut Self{
        self.app_name = app_name;
        self
    }

    /// Set the broker for the application. Only one is allowed. You can create many clients
    /// but should be able to have a limited number of workers. This reduces resources and
    /// makes applications easier to manage but may change later. Computing resources should not
    /// need to scale massively but applications can terminate as they are not going to store long
    /// running tools and frameworks.
    ///
    /// # Arguments
    /// * `broker` - Broker for the application
    pub fn broker(&mut self, broker: BrokerType) -> &mut Self{
        self.broker = Some(broker);
        self
    }

    /// Set the backend type. One is allowed for easier management for now. Not sure if more than
    /// one is necessary.
    ///
    /// # Arguments
    /// * `backend` - Available backend for the application
    pub fn backend(&mut self, backend: AvailableBackend) -> &mut Self{
        self.backend = Some(backend);
        self
    }

    /// Set the data cannon configuration containing global variables common to our applications.
    ///
    /// # Arguments
    /// * `config` - The configuration
    pub fn config(&mut self, config: CannonConfig<'static>) -> &mut Self{
        self.config = Some(config);
        self
    }

    /// Set the configuration containing client specific variables
    ///
    /// # Arguments
    /// * `config` - General configuration for our application
    pub fn general_config(&mut self, config: GeneralConfig) -> &mut Self{
        self.general_config = config;
        self
    }

    /// Set the backend consumer ids
    pub fn backend_consumer_ids(&mut self, ids: HashSet<String>) -> &mut Self{
        self.backend_consumer_ids = Some(ids);
        self
    }

    /// Create logger formatting
    ///
    /// # Arguments
    /// * `level` - Logging level filter
    fn create_logger(&self, level : LevelFilter){
        Builder::new()
            .format(|buf, record| {
                writeln!(buf,
                         "{} [{}] - {}",
                         Local::now().format("%Y-%m-%dT%H:%M:%S"),
                         record.level(),
                         record.args()
                )
            })
            .filter(None, level)
            .init();
    }

    /// Convert set to vector
    ///
    /// # Arguments
    /// * `set` - The ids set
    fn get_id_vec(&self, set: HashSet<String>) -> Vec<String>{
        let mut vec = vec![];
        for v in set{
            vec.push(v);
        }
        vec
    }

    /// Build an application config from provided variables. Tries to unwrap the options
    /// wrapping each variable. Will throw an error if empty.
    ///
    /// # Arguments
    /// * `rt` - The runtime to use for the appliation
    /// * `level` - Logging level
    pub async fn build(self, level: Option<LevelFilter>, rt: Arc<Runtime>) -> Result<App, ()>{
        let filter = level.unwrap_or(LevelFilter::Info);
        self.create_logger(filter);
        info!("Datacannon :: client :: Creating Broker");
        let broker_result = (&self).create_broker_handler(rt).await;
        info!("Datacannon :: client :: Creating Backend");
        let backend_result = (&self).create_backend_handler(
            self.backend_consumer_ids.clone()).await;
        let (backend, ids) = backend_result.ok().unwrap();
        let broker = broker_result.ok().unwrap();
        let id_vec = self.get_id_vec(ids.unwrap());
        debug!("Datacannon :: client :: Generating Application @ {}", self.host_name.clone());
        let app = App{
            registry: HashSet::new(),
            app_name: self.app_name,
            host_name: self.host_name,
            backend: self.backend.unwrap(),
            backend_consumer_ids: id_vec,
            broker: self.broker.unwrap(),
            config: self.config.unwrap(),
            general_config: self.general_config,
            client_backend: RefCell::new(Some(backend)),
            client_broker: RefCell::new(Some(broker)),
            result_store: self.result_store
        };
        Ok(app)
    }

    /// Start the backend handler. Returns a RedisResultHandler and final ids or an error.
    ///
    /// # Arguments
    /// * `ids` - Ids for the consumers
    async fn create_backend_handler(
        &self, ids: Option<HashSet<String>>) -> Result<(ClientBackend, Option<HashSet<String>>), ()>{
        let mut bids = ids;
        let config = self.config.clone().unwrap();
        let backend_tup = self.backend.clone();
        let app_status = self.general_config.get_app_status().clone();
        let result = match backend_tup{
            Some(AvailableBackend::Redis(backend_tup)) => {
                let (cfg, num_handles) = backend_tup;
                debug!("Datacannon:: client :: Starting {} redis handles", num_handles);
                if bids.is_none(){
                    let mut idset = HashSet::new();
                    let hname = get_hostname_str();
                    let app_name = self.app_name.clone();
                    for _i in 0..num_handles.clone(){
                        debug!("Datacannon :: client :: Redis consumer route will be {}", hname);
                        let uid = Uuid::new_v4();
                        let id = format!("{}_redis_consumer_{}@{}", app_name, uid, hname);
                        idset.insert(id);
                    }
                    bids = Some(idset);
                }
                let handler = RedisResultHandler::new(
                    config,
                    app_status,
                    cfg,
                    num_handles as usize,
                    bids.clone(),
                    self.result_store.clone()).await;
                debug!("Datacannon :: client :: Redis Consumers Ready");
                Ok((ClientBackend::Redis(handler), bids))
            }
            _ =>{
                Err(())
            }
        };
        result
    }

    /// Start the broker handler
    async fn create_broker_handler(&self, rt: Arc<Runtime>) -> Result<ClientBroker, ()>{
        let config = self.config.clone().unwrap();
        let broker_type = self.broker.clone().take();
        match broker_type{
            Some(BrokerType::RABBITMQ) =>{
                let routers = self.config.clone().unwrap().routers.clone();
                let broker = RabbitMQBroker::new(
                    config, routers, self.result_store.clone(), rt).await;
                if broker.is_ok() {
                    Ok(ClientBroker::RabbitMQ(broker.ok().unwrap()))
                }else{
                    Err(())
                }
            }
            _ =>{
                Err(())
            }
        }
    }

    /// Setup a default app builder. You must fill in each item.
    pub fn default() -> AppBuilder{
        let hname = get_hostname_str();
        AppBuilder{
            app_name: "data_cannon_client",
            host_name: hname,
            broker: None,
            backend: None,
            backend_consumer_ids: None,
            config: None,
            general_config: GeneralConfig::new(),
            result_store: Arc::new(RwLock::new(HashMap::new()))
        }
    }
}


/// Application used to run
#[allow(dead_code)]
pub struct App{
    registry: HashSet<&'static str>,
    app_name: &'static str,
    host_name: String,
    backend: AvailableBackend,
    backend_consumer_ids: Vec<String>,
    broker: BrokerType,
    config: CannonConfig<'static>,
    general_config: GeneralConfig,
    client_broker: RefCell<Option<ClientBroker>>,
    client_backend: RefCell<Option<ClientBackend>>,
    result_store: Arc<RwLock<HashMap<String, Arc<TokioMutex<Sender<TaskResponse>>>>>>
}


/// Implement the builder
impl App{

    /// Get the broker type for the client broker.
    fn get_broker_type(&mut self) -> Result<BrokerType, ()> {
        let mut bref = self.client_broker.borrow_mut();
        let broker_opt = bref.as_mut().take();
        match broker_opt {
            Some(ClientBroker::RabbitMQ(_broker)) => {
                Ok(BrokerType::RABBITMQ)
            }
            _ =>{
                Err(())
            }
        }
    }

    /// Register a task. Ensure that works attached to your queues can handle
    /// this task. There are two two types of tasks and three types of pools.
    /// Make sure you understand which to use in your worker.
    ///
    /// # Arguments
    /// * `task_name` - Task name, same as the name on your server
    pub fn register_task(&mut self, task_name: &'static str){
        self.registry.insert(task_name);
    }

    /// Submit a task for the application. Return an asynchronous result. You
    /// can use the TaskConfigBuilder or StreamConfigBuilder to create each.
    ///
    /// # Arguments
    /// * `task_config` - The task configuration
    /// * `stream_config` - Stream configuration
    /// * `errbacks` - Error callbacks for the task
    /// * `callbacks` - Callbacks for the task
    pub async fn submit_task(
        &mut self,
        task_config: TaskConfig,
        stream_config: Option<StreamConfig>,
        errbacks: Option<Vec<Errback>>,
        callbacks: Option<Vec<Callback>>) -> Result<AsyncResult, ()> {
        let keys = self.registry.clone();
        let task_name = task_config.get_task_name().clone();
        if keys.contains(task_name.as_str()) {
            let cfg = self.config.clone();
            let mut bref = self.client_broker.borrow_mut();
            let broker = bref.take();
            match broker {
                Some(ClientBroker::RabbitMQ(mut broker)) => {
                    let task = broker.send_task(
                        cfg,
                        task_config,
                        stream_config,
                        None,
                        self.backend.clone()).await;
                    if task.is_ok() {
                        let mut result = task.ok().unwrap();
                        if errbacks.is_some() {
                            result.errbacks = errbacks.unwrap();
                        }
                        if callbacks.is_some() {
                            result.callbacks = callbacks.unwrap();
                        }
                        drop(bref);
                        self.client_broker.replace(Some(ClientBroker::RabbitMQ(broker)));
                        Ok(result)
                    } else {
                        Err(())
                    }
                }
                _ => {
                    Err(())
                }
            }
        }else{
            Err(())
        }
    }

    /// Convert the ExchangeKind to a string
    fn get_default_exchange_type(&self) -> String{
        let ekind = self.config.default_exchange_type.clone();
        match ekind{
            ExchangeKind::Direct => {
                "DIRECT".to_string()
            },
            ExchangeKind::Fanout => {
                "FANOUT".to_string()
            },
            ExchangeKind::Headers => {
                "HEADERS".to_string()
            },
            ExchangeKind::Topic => {
                "TOPIC".to_string()
            }
            ExchangeKind::Custom(ekind) => {
                ekind.clone()
            }
        }
    }


    /// Provides a builder for the task builder using some of the provided variables. You can
    /// change different variables such as:
    ///     - result_expires
    ///     - parent_id
    ///     - reply_to
    ///     - routing_key
    ///     - exchange
    /// Call the build method when done.
    ///
    /// # Arguments
    /// * `task_name` - Name of the task
    /// * `args` - Argument vector
    /// * `kwawrgs` - Mapped arguments
    /// * `expires` - Result expiration time
    /// * `time_limit`: Time
    pub fn create_task_builder(
        &mut self,
        task_name: String,
        args: Option<Vec<ArgType>>,
        kwargs: Option<HashMap<String, ArgType>>,
        expires: Option<u64>,
        soft_time_limit: Option<u64>,
        time_limit: Option<u64>) -> Result<TaskConfigBuilder, ()>{
        let argv = args.unwrap_or(vec![]);
        let kwarg_map = kwargs.unwrap_or(HashMap::new());
        let parent_id = format!(
            "{}_{}@{}", Uuid::new_v4(), self.app_name, self.host_name);
        let corr_id = format!("{}", Uuid::new_v4());
        let broker_type = self.get_broker_type();
        let default_expires = self.config.task_result_expires.clone() as u64;
        let task_expires = expires.unwrap_or(default_expires);
        let soft_limit = soft_time_limit.unwrap_or(default_expires.clone());
        let hard_time_limit = time_limit.unwrap_or(default_expires.clone());
        if broker_type.is_ok() {
            let b = broker_type.ok().unwrap();
            let broker_string = b.to_string();
            let mut builder = TaskConfigBuilder::default();
            let id = self.backend_consumer_ids.pop().unwrap();
            self.backend_consumer_ids.push(id.clone());
            builder
                .broker(broker_string.to_string())
                .task_name(task_name)
                .args(argv)
                .kwargs(kwarg_map)
                .reply_to(id)
                .parent_id(parent_id)
                .correlation_id(corr_id)
                .result_expires(task_expires)
                .priority(0)
                .time_limit(hard_time_limit)
                .soft_time_limit(soft_limit)
                .eta(1000000 as u64)
                .retries(self.config.task_retries)
                .lang("rust".to_string())
                .shadow("datacannon".to_string())
                .exchange(Some(self.config.default_exchange.to_string()))
                .exchange_type(Some(self.get_default_exchange_type()))
                .routing_key(Some(self.config.default_routing_key.to_string()));
            Ok(builder.to_owned())
        }else {
            Err(())
        }
    }

    /// Close the backend
    async fn close_backend(&self){
        let mut bref =  self.client_backend.borrow_mut();
        let backend = bref.as_mut();
        match backend{
            Some(ClientBackend::Redis(backend)) => {
                backend.close().await;
            }
            _ => {}
        }
    }

    /// Close the broker
    async fn close_broker(&self){
        let mut bref = self.client_broker.borrow_mut();
        let broker_opt = bref.take();
        if broker_opt.is_some() {
            let broker = broker_opt.unwrap();
            match broker {
                ClientBroker::RabbitMQ(mut broker) => {
                    broker.close().await;
                }
            }
        }
    }

    /// close the application. Stops the broker and backend.
    pub async fn close(&self) {
        self.close_backend().await;
        self.close_broker().await;
    }
}


#[cfg(test)]
pub mod tests{
    use std::env;

    use datacannon_rs_core::backend::config::BackendConfig;
    use datacannon_rs_core::backend::redis::producer;
    use datacannon_rs_core::backend::redis::producer::RedisProducer;
    use datacannon_rs_core::backend::types::AvailableBackend;
    use datacannon_rs_core::broker::broker_type;
    use datacannon_rs_core::config::config::{BackendType, BrokerType, CannonConfig};
    use datacannon_rs_core::connection::amqp::connection_inf::AMQPConnectionInf;
    use datacannon_rs_core::connection::connection::ConnectionConfig;
    use datacannon_rs_core::message_protocol::stream::StreamConfig;
    use datacannon_rs_core::message_structure::amqp::exchange::Exchange;
    use datacannon_rs_core::message_structure::amqp::queue::AMQPQueue;
    use datacannon_rs_core::message_structure::queues::GenericQueue;
    use datacannon_rs_core::replication::rabbitmq::{RabbitMQHAPolicies, RabbitMQHAPolicy};
    use datacannon_rs_core::replication::replication::HAPolicy;
    use datacannon_rs_core::result::asynchronous::ResultType;
    use datacannon_rs_core::router::router::{Router, Routers};
    use datacannon_rs_core::task::result::{Response, TaskResponse};
    use lapin::ExchangeKind;
    use tokio::runtime::Runtime;

    use super::*;

    fn get_routers() -> Routers {
        let mut rts = Routers::new();
        let user = env::var("RABBITMQ_USERNAME").unwrap();
        let pwd = env::var("RABBITMQ_PWD").unwrap();
        let mut queues = Vec::<GenericQueue>::new();
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(),
            "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some(user),
            Some(pwd),
            1000);
        let q = AMQPQueue::new(
            "lapin_test_queue".to_string(), Some("test_exchange".to_string()), Some("test_route".to_string()), 0, HAPolicy::RabbitMQ(RabbitMQHAPolicy::new(RabbitMQHAPolicies::ALL, 1)), false, amq_conf);
        queues.push(GenericQueue::AMQPQueue(q));
        let exch = Exchange::new("test_exchange".to_string(), ExchangeKind::Direct);
        let router = Router::new("test_key".to_string(), queues, Some(exch));
        rts.add_router("test_key".to_string(), router);
        rts
    }

    fn get_config<'a>() -> CannonConfig<'a>{
        let user = env::var("RABBITMQ_USERNAME");
        let pwd = env::var("RABBITMQ_PWD");
        assert!(pwd.is_ok());
        assert!(user.is_ok());
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(), "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some(user.ok().unwrap()),
            Some(pwd.ok().unwrap()),
            1000);
        let conn_conf = ConnectionConfig::RabbitMQ(amq_conf);
        let backend_conf = get_backend_config();
        let routers = get_routers();
        let mut cannon_conf = CannonConfig::new(
            conn_conf, BackendType::REDIS, backend_conf, routers);
        cannon_conf.num_broker_channels = 30;
        cannon_conf.consumers_per_queue = 1;
        cannon_conf.num_backend_connections = 2;
        cannon_conf.default_routing_key = "test_key";
        cannon_conf.default_exchange = "test_exchange";
        cannon_conf.default_exchange_type = ExchangeKind::Direct;
        cannon_conf
    }

    fn get_stream_config() -> StreamConfig{
        StreamConfig::new(None, None)
    }

    fn get_backend_config() -> BackendConfig {
        let bc = BackendConfig {
            url: "127.0.0.1:6379",
            username: None,
            password: None,
            transport_options: None
        };
        bc
    }

    fn get_runtime() -> Arc<Runtime>{
        let rt = tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .worker_threads(10)
            .build()
            .unwrap();
        Arc::new(rt)
    }

    fn get_task_config(parent_id: String) -> TaskConfig{
        let config = get_config();
        TaskConfig::new(
            config,
            broker_type::RABBITMQ.to_string(),
            "test_it".to_string(),
            Some("test_exchange".to_string()),
            Some("DIRECT".to_string()),
            Some("test_key".to_string()),
            Some(parent_id),
            None,
            None,
            Some("test_1".to_string()),
            None,
            None,
            None,
            None,
            None,
            10000,
            Some(1),
            None,
            None)
    }

    async fn get_app(rt: Arc<Runtime>) -> Result<App, ()> {
        let cfg = get_config();
        let backend_cfg = get_backend_config();
        let backend = AvailableBackend::Redis((backend_cfg, 10));
        let mut ids = HashSet::<String>::new();
        ids.insert("test_1".to_string());
        let mut builder = AppBuilder::default();
        builder.config(cfg);
        builder.broker(BrokerType::RABBITMQ);
        builder.backend(backend);
        builder.app_name("test_app");
        builder.backend_consumer_ids(ids);
        builder.build(Some(LevelFilter::Debug), rt).await
    }

    async fn send_fake_response(parent_id: String){
        let task = get_task_config(parent_id);
        let stream_config = get_stream_config();
        let conf = get_backend_config();
        let prod = RedisProducer::new(conf).await;
        let pconn = prod.get_paired_conn();
        let mut response = Response::new();
        response.add_result("ok".to_string(), ArgType::Bool(true));
        response.add_result("hello".to_string(), ArgType::String("world".to_string()));
        response.add_result("test_result".to_string(), ArgType::Bool(true));
        let task_response = TaskResponse::new(
            task, response, stream_config, true, None);
        let msgr = serde_json::to_string(&task_response);
        assert!(msgr.is_ok());
        let msg = msgr.ok().unwrap();
        let r = producer::publish(
            pconn, Some(1), Some(10), "test_1".to_string(), msg).await;
        assert!(r.0);
    }

    /// Check a response
    ///
    /// # Arguments
    /// * `response_opt` - Response from the backend
    fn check_worker_response(response_opt: Option<TaskResponse>){
        assert!(response_opt.is_some());
        let task_response = response_opt.unwrap();
        let response = task_response.get_response().clone();
        assert!(task_response.is_ok());
        let rmap = response.get_map();
        assert!(rmap.contains_key("test_result"));
        let ok = response.get_result("test_result").unwrap();
        if let ArgType::Bool(ok) = ok{
            assert!(ok);
        }else{
            panic!("Response type was not bool");
        }
    }

    /// Check a response
    ///
    /// # Arguments
    /// * `response_opt` - Response from the backend
    fn check_response(response_opt: Option<TaskResponse>){
        assert!(response_opt.is_some());
        let task_response = response_opt.unwrap();
        let response = task_response.get_response().clone();
        let ok = response.get_result("test_result").unwrap();
        if let ArgType::Bool(ok) = ok{
            assert!(ok);
        }else{
            panic!("Response type was not bool");
        }
    }

    async fn setup_broker(rt: Arc<Runtime>){
        let config = get_config();
        let results = Arc::new(RwLock::new(HashMap::new()));
        let routers = get_routers();
        let broker_result = RabbitMQBroker::new(
            config, routers, results, rt).await;
        assert!(broker_result.is_ok());
        let mut broker = broker_result.ok().unwrap();
        let _r = broker.setup().await;
        broker.close().await
    }

    #[test]
    fn should_setup_app(){
        let rt = get_runtime();
        rt.clone().block_on(async move {
            let b = get_app(rt).await;
            assert!(b.is_ok());
            let app = b.unwrap();
            app.close().await;
        });
    }

    #[test]
    fn should_send_task(){
        let rt = get_runtime();
        rt.clone().block_on(async move {
            let b = get_app(rt).await;
            let sc = get_stream_config();
            assert!(b.is_ok());
            let mut app = b.ok().unwrap();
            let task_builder = app.create_task_builder(
                "test_it".to_string(),
                None,
                None,
                None,
                Some(10000),
                Some(100000)).ok().unwrap().build();
            let task = task_builder.ok().unwrap();
            app.register_task("test_it");
            let parent_id = task.get_parent_id().clone();
            let result = app.submit_task(
                task, Some(sc), None, None).await;
            let mut async_result = result.ok().unwrap();
            send_fake_response(parent_id).await;
            let await_result = async_result.await_send().await;
            assert!(await_result.is_ok());
            app.close().await
        });
    }

    #[test]
    fn should_retreive_result(){
        let rt = get_runtime();
        rt.clone().block_on(async move {
            setup_broker(rt.clone()).await;
            let b = get_app(rt).await;
            let sc = get_stream_config();
            assert!(b.is_ok());
            let mut app = b.ok().unwrap();
            let task_builder = app.create_task_builder(
                "test_it".to_string(),
                None,
                None,
                None,
                Some(10000),
                Some(100000)).ok().unwrap().build();
            let task: TaskConfig = task_builder.ok().unwrap();
            assert!(task.get_reply_to().eq("test_1"));
            app.register_task("test_it");
            let parent_id = task.get_parent_id().clone();
            let result = app.submit_task(
                task, Some(sc), None, None).await;
            let mut async_result = result.ok().unwrap();
            let await_send = async_result.await_send().await;
            assert!(await_send.is_ok());
            send_fake_response(parent_id).await;
            let await_result = async_result.await_result().await;
            assert!(await_result.is_ok());
            check_response(await_result.ok());
            app.close().await;
        });
    }

    #[test]
    fn should_complete_entire_result(){
        let sc = get_stream_config();
        let rt = get_runtime();
        rt.clone().block_on(async move {
            let b = get_app(rt).await;
            assert!(b.is_ok());
            let mut app = b.ok().unwrap();
            let task_builder = app.create_task_builder(
                "test_it".to_string(),
                None,
                None,
                None,
                Some(10000),
                Some(100000)).ok().unwrap().build();
            let task = task_builder.ok().unwrap();
            let parent_id = task.get_parent_id().clone();
            app.register_task("test_it");
            let result = app.submit_task(
                task, Some(sc), None, None).await;
            let mut async_result = result.ok().unwrap();
            send_fake_response(parent_id).await;
            let result = async_result.complete().await;
            assert!(result.is_ok());
            let rtype = result.ok().unwrap();
            if let ResultType::RESPONSE(r) = rtype {
                let response_opt = r.get_response().clone();
                check_response(response_opt);
            } else {
                panic!("Result Type Not Response");
            }
            app.close().await;
        });
    }

    /// Requires worker
    #[test]
    fn should_integrate_with_worker(){
        let sc = get_stream_config();
        let rt = get_runtime();
        rt.clone().block_on(async move {
            let b = get_app(rt).await;
            assert!(b.is_ok());
            let mut app = b.ok().unwrap();
            let task_builder = app.create_task_builder(
                "test_it".to_string(),
                None,
                None,
                None,
                Some(10000),
                Some(100000)).ok().unwrap().build();
            let task = task_builder.ok().unwrap();
            app.register_task("test_it");
            let result = app.submit_task(
                task, Some(sc), None, None).await;
            let mut async_result = result.ok().unwrap();
            let result = async_result.complete().await;
            assert!(result.is_ok());
            let rtype = result.ok().unwrap();
            if let ResultType::RESPONSE(r) = rtype {
                let response_opt = r.get_response().clone();
                check_worker_response(response_opt);
            } else {
                panic!("Result Type Not Response");
            }
            app.close().await;
        });
    }

    /// Requires worker
    #[test]
    fn should_execute_chord_on_worker(){
        let rt = get_runtime();
        rt.clone().block_on(async move {
            let b = get_app(rt).await;
            assert!(b.is_ok());
            let mut app = b.ok().unwrap();
            let task_builder = app.create_task_builder(
                "test_it".to_string(),
                None,
                None,
                None,
                Some(10000),
                Some(100000)).ok().unwrap().build();
            let task = task_builder.ok().unwrap();
            let mut chord = vec![];
            chord.push(task.clone());
            chord.push(task.clone());
            chord.push(task.clone());
            let sc = StreamConfig::new(Some(chord), None);
            app.register_task("test_it");
            let result = app.submit_task(
                task, Some(sc), None, None).await;
            let mut async_result = result.ok().unwrap();
            let result = async_result.complete().await;
            assert!(result.is_ok());
            let rtype = result.ok().unwrap();
            if let ResultType::RESPONSE(r) = rtype {
                let response_opt = r.get_response().clone();
                check_worker_response(response_opt);
            } else {
                panic!("Result Type Not Response");
            }
            app.close().await;
        });
    }

    /// Requires worker
    #[test]
    fn should_execute_chain_on_worker(){
        let rt = get_runtime();
        rt.clone().block_on(async move {
            let b = get_app(rt).await;
            assert!(b.is_ok());
            let mut app = b.ok().unwrap();
            let task_builder = app.create_task_builder(
                "test_it".to_string(),
                None,
                None,
                None,
                Some(10000),
                Some(100000)).ok().unwrap().build();
            let task = task_builder.ok().unwrap();
            let mut chord = vec![];
            chord.push(task.clone());
            chord.push(task.clone());
            chord.push(task.clone());
            let chord2 = chord.clone();
            let sc3 = StreamConfig::new(Some(chord2), None);
            let sc4 = sc3.clone();
            let sc2 = StreamConfig::new(Some(chord), Some(vec![sc3]));
            let sc = StreamConfig::new(None, Some(vec![sc2, sc4]));
            app.register_task("test_it");
            let result = app.submit_task(
                task, Some(sc), None, None).await;
            let mut async_result = result.ok().unwrap();
            let result = async_result.complete().await;
            assert!(result.is_ok());
            let rtype = result.ok().unwrap();
            if let ResultType::RESPONSE(r) = rtype {
                let response_opt = r.get_response().clone();
                check_worker_response(response_opt);
            } else {
                panic!("Result Type Not Response");
            }
            app.close().await;
        });
    }

    #[test]
    fn should_submit_task_asynchronously(){
        let rt = get_runtime();
        rt.clone().block_on(async move {
            let b = get_app(rt).await;
            assert!(b.is_ok());
            let mut app = b.ok().unwrap();
            let task_builder = app.create_task_builder(
                "test_it".to_string(),
                None,
                None,
                None,
                Some(10000),
                Some(100000)).ok().unwrap().build();
            let task = task_builder.ok().unwrap();
            let mut chord = vec![];
            chord.push(task.clone());
            chord.push(task.clone());
            chord.push(task.clone());
            let chord2 = chord.clone();
            let sc3 = StreamConfig::new(Some(chord2), None);
            let sc4 = sc3.clone();
            let sc2 = StreamConfig::new(Some(chord), Some(vec![sc3]));
            let sc = StreamConfig::new(None, Some(vec![sc2, sc4]));
            app.register_task("test_it");
            let result = app.submit_task(
                task, Some(sc), None, None).await;
            let mut async_result = result.ok().unwrap();
            let result = async_result.complete().await;
            assert!(result.is_ok());
            let rtype = result.ok().unwrap();
            if let ResultType::RESPONSE(r) = rtype {
                let response_opt = r.get_response().clone();
                check_worker_response(response_opt);
            } else {
                panic!("Result Type Not Response");
            }
            app.close().await;
        });
    }

    #[test]
    fn test_at_load(){
        let rt = get_runtime();
        rt.clone().block_on(async move {
            let b = get_app(rt).await;
            let sc = get_stream_config();
            assert!(b.is_ok());
            let mut app = b.ok().unwrap();
            app.register_task("test_it");
            let mut results = Vec::<AsyncResult>::new();
            for _i in 0..50000 as i32{
                let task_builder = app.create_task_builder(
                    "test_it".to_string(),
                    None,
                    None,
                    None,
                    Some(10000),
                    Some(100000)).ok().unwrap().build();
                let task = task_builder.ok().unwrap();
                let result = app.submit_task(
                    task.clone(),
                    Some(sc.clone()),
                    None,
                    None).await;
                let async_result = result.ok().unwrap();
                results.push(async_result);
            }
            for mut async_result in results {
                let result = async_result.complete().await;
                assert!(result.is_ok());
                let rtype = result.ok().unwrap();
                if let ResultType::RESPONSE(r) = rtype {
                    let response_opt = r.get_response().clone();
                    check_worker_response(response_opt);
                } else {
                    panic!("Result Type Not Response");
                }
            }
            app.close().await;
        });
    }
}