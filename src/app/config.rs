//! The general configuration for the client containing client specific information
//!
//!
//! ---
//! author: Andrew Evans
//! ---

use std::sync::Arc;
use std::sync::atomic::AtomicBool;

/// Structure containing global client related variables
pub struct GeneralConfig{
    app_status: Arc<AtomicBool>
}


/// Implementation of general config
impl GeneralConfig{

    /// Get the application status AtomicBool
    pub(crate) fn get_app_status(&self) -> Arc<AtomicBool>{
        self.app_status.clone()
    }

    /// Create a new general client configuration
    pub fn new() -> GeneralConfig{
        let status = Arc::new(AtomicBool::new(true));
        GeneralConfig{
            app_status: status
        }
    }
}
