//! Broker application
//!
//! ---
//! author: Andrew Evans
//! ---

use datacannon_rs_core::broker::amqp::rabbitmq::RabbitMQBroker;

pub enum ClientBroker{
    RabbitMQ(RabbitMQBroker)
}
