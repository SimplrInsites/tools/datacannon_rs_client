//! Backend enumeration to store our backend
//!
//! ---
//! author: Andrew Evans
//! ---

use datacannon_rs_core::backend::redis::backend::RedisResultHandler;

/// Client backend storage
pub enum ClientBackend{
    Redis(RedisResultHandler)
}
