//! Host name uitilties
//!
//! ---
//! author: Andrew Evans
//! ---

use hostname;

/// Create a standardized hostname
pub(crate) fn get_hostname_str() -> String{
    let mut hname = "data_cannon_client".to_string();
    let hnamer = hostname::get();
    if hnamer.is_ok(){
        let hname_os = hnamer.ok().unwrap();
        let hstr_opt = hname_os.to_str();
        if hstr_opt.is_some() {
            hname = hstr_opt.unwrap().to_string();
        }
    }
    hname
}