//! Stream builder  for tasks. Streams are composed of chords (run with initial task) and
//! chains (sent back to the broker from the worker with the results). This is to be created
//! in addition to the task sent to the broker.
//!
//! Chords contain tasks while chains are made of additional stream configs.
//!
//!
//! ---
//! author: Andrew Evans
//! ---

use datacannon_rs_core::message_protocol::stream::StreamConfig;
use datacannon_rs_core::task::config::TaskConfig;


/// Append a part of the stream to the chain. Returns the manipulated config. Will ignore
/// any configs without tasks
///
/// # Arguments
/// * `config` - The Stream Config
/// * `chain_config` - Chain configuration
pub fn enqueue_in_chain(config: StreamConfig, chain_config: StreamConfig) -> StreamConfig{
    if chain_config.chord.is_empty() == false {
        let mut scfg = config;
        scfg.chain.push(chain_config);
        scfg
    }else{
        config
    }
}


/// Add a task to the chord in the config. Returns the manipulated config
///
/// # Arguments
/// * `config` - The StreamConfig
/// * `chord_task` - The chord task
pub fn add_chord_task(config: StreamConfig, chord_task: TaskConfig) -> StreamConfig{
    let mut stream = config;
    stream.chord.push(chord_task);
    stream
}


#[cfg(test)]
pub mod test{
    use std::env;

    use datacannon_rs_core::backend::config::BackendConfig;
    use datacannon_rs_core::broker::broker_type;
    use datacannon_rs_core::config::config::{BackendType, CannonConfig};
    use datacannon_rs_core::connection::amqp::connection_inf::AMQPConnectionInf;
    use datacannon_rs_core::connection::connection::ConnectionConfig;
    use datacannon_rs_core::message_structure::amqp::exchange::Exchange;
    use datacannon_rs_core::message_structure::amqp::queue::AMQPQueue;
    use datacannon_rs_core::message_structure::queues::GenericQueue;
    use datacannon_rs_core::replication::rabbitmq::{RabbitMQHAPolicies, RabbitMQHAPolicy};
    use datacannon_rs_core::replication::replication::HAPolicy;
    use datacannon_rs_core::router::router::{Router, Routers};
    use lapin::ExchangeKind;

    use super::*;

    fn get_routers() -> Routers {
        let mut rts = Routers::new();
        let mut queues = Vec::<GenericQueue>::new();
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(),
            "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some("dev".to_string()),
            Some("rtp*4500".to_string()),
            1000);
        let q = AMQPQueue::new(
            "lapin_test_queue".to_string(),
            Some("test_exchange".to_string()),
            Some("test_route".to_string()),
            0,
            HAPolicy::RabbitMQ(RabbitMQHAPolicy::new(RabbitMQHAPolicies::ALL, 1)),
            false,
            amq_conf);
        queues.push(GenericQueue::AMQPQueue(q));
        let exch = Exchange::new("test_exchange".to_string(), ExchangeKind::Direct);
        let router = Router::new("test_key".to_string(), queues, Some(exch));
        rts.add_router("test_key".to_string(), router);
        rts
    }

    fn get_config<'a>() -> CannonConfig<'a>{
        let user = env::var("rabbit_test_user");
        let pwd = env::var("rabbit_test_pwd");
        assert!(pwd.is_ok());
        assert!(user.is_ok());
        let amq_conf = AMQPConnectionInf::new(
            "amqp".to_string(), "127.0.0.1".to_string(),
            5672,
            Some("test".to_string()),
            Some(user.ok().unwrap()),
            Some(pwd.ok().unwrap()),
            1000);
        let conn_conf = ConnectionConfig::RabbitMQ(amq_conf);
        let backend_conf = get_backend_config();
        let routers = get_routers();
        let mut cannon_conf = CannonConfig::new(
            conn_conf, BackendType::REDIS, backend_conf, routers);
        cannon_conf.num_broker_channels = 30;
        cannon_conf.consumers_per_queue = 25;
        cannon_conf.default_routing_key = "test_key";
        cannon_conf.default_exchange = "test_exchange";
        cannon_conf.default_exchange_type = ExchangeKind::Direct;
        cannon_conf
    }

    fn get_test_task() -> TaskConfig{
        let config = get_config();
        TaskConfig::new(
            config,
            broker_type::RABBITMQ.to_string(),
            "test_it".to_string(),
            Some("test_exchange".to_string()),
            Some("DIRECT".to_string()),
            Some("test_key".to_string()),
            Some("test_parent_id".to_string()),
            None,
            None,
            Some("test_1".to_string()),
            None,
            None,
            None,
            None,
            None,
            10000,
            Some(1),
            None,
            None)
    }

    fn get_backend_config() -> BackendConfig {
        let bc = BackendConfig {
            url: "127.0.0.1:6379",
            username: None,
            password: None,
            transport_options: None
        };
        bc
    }

    fn get_empty_stream() -> StreamConfig{
        StreamConfig::new(None, None)
    }

    #[test]
    fn should_add_task_to_chord(){
        let mut sc = get_empty_stream();
        let task = get_test_task();
        sc = add_chord_task(sc, task);
        assert_eq!(sc.chord.len(), 1);
    }

    #[test]
    fn should_add_config_to_chain(){
        let mut sc = get_empty_stream();
        let mut sc2 = get_empty_stream();
        let task = get_test_task();
        sc2.chord.push(task);
        assert_eq!(sc2.chord.len() , 1);
        sc = enqueue_in_chain(sc, sc2);
        assert_eq!(sc.chain.len(), 1);
    }

    #[test]
    fn should_not_append_an_empty_config(){
        let mut sc = get_empty_stream();
        let sc2 = get_empty_stream();
        sc = enqueue_in_chain(sc, sc2);
        assert_eq!(sc.chain.len(), 0);
    }
}